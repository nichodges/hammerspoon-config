# Hammerspoon Config
## Example Configs
https://github.com/Hammerspoon/hammerspoon/wiki/Sample-Configurations

Cofig loading is based on: https://github.com/tstirrat/hammerspoon-config

## TODO
- [x] Make it go through all windows and centre/scale for big screen
- [x] Spectacle Clone: https://github.com/miromannino/hammerspoon-config
- [x] Pomodoro
- [x] Setup config files correctly
- [ ] Centre a window
- [ ] Set Pomodoro to remove previous notification
- [ ] Flux
- [ ] Hide Hammerspoon icon in menubar and enable all with shortcuts
- [ ] When open and on home WiFi, Sync random videos folder
- [ ] Auto-connect Viscosity if not on whitelist of WiFi
- [ ] All window resize based on WiFi network (ie. Home vs. Work)
- [ ] Add copied URL to youtube-dl
- [ ] Menu bar / Notifications for processes (daily/weekly)
- [ ] Something that takes a URL in the clipboard, and converts to a shortened URL (Bit.ly or something less tracky)
