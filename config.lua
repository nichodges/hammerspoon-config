local config = {}

config.modules = {
    "auto-reload",
    "pomodoro",
    "spectacles"
}


return config